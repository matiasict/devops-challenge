# Challenge

## Terraform:

`./terraform-infrastructure`

En la carpeta terraform-infrastructure encontramos la definicion de infraestructura.
Se utilizo la version de terraform v1.2.9 junto al provider AWS 4.0. El backend se encuentra remoto, dentro del s3 y con su lock table correspondiente.

ECR (Registry donde guardamos nuestra imagen nginx)
VPC (El modulo de VPC tambien se encarga de deployar todo lo relacionado a networking (Subnets, SG, DNS support, etc ))
Definicion de Backend para el remote state.
EKS (cluster de kubernetes).

## Terraform-modules:

`./terraform-modules`

Econtramos los modulos de backend y VPC.

## Docker-image:

`./docker-image`

Encontramos la definicion del container docker y el sitio web.

## Helfile-proyect:

`./helmfile-project`

Encontramos las definiciones para poder desplegar nuestra imagen de nginx customizada en el cluster de kubernetes por medio de Helmfile

Los comandos son:

- `helmfile -e dev apply`
- `helmfile -e stage apply`

![Diagram](./src/diagram.png)

## Charts:

`./charts`

Encontramos la definiciones de nuestro chart, el mismo realiza el pull de nuestra imagen nginx del ECR.

## Pipeline:

Dentro de cada folder encontramos la definicion de cada pipeline.
Terraform: Genera un plan y apply (este ultimo tiene que ser ejecutado a mano)
Docker-image: Genera la imagen de nginx con nuestro sitio dentro, y lo publica en el ECR

# Teórico

### Terraform

1. Los modulos terraform nos permiten crear infraestrctura de un modo rapido y sensillo. Ademas, pueden ser reutilizado para varios entornos o aplicaciones de codigo. Ejemplo, si contamos con el modulo de lambdas, se nos va a hacer mucho mas rapido el despliegue de varias lambdas, ya que la definicion de los resources estan en el modulo. Solo cambiando un par de variables podemos desplegar infinita cantidad de lambdas.

2. Primero reviso el readme de cada modulo revisando que inputs son necesarios y obligatorios, luego llamo al modulo, completo esos inputs y por medio de un terraform validate reviso que este ok una vez que este ok genero el plan.

### Docker

1. Todas imagenes docker estan compuestas por layers. Los leyers se van apilando para conformar una imagen. Estos pueden ser: Nuevo fiecheros respecto al layer anteroir, Ficheros que cambian, ficheros que se eliminan, etc.

2. Para optimziar las imagenes: Es recomenando utilizar la menor cantidad de layer posibles tambien es una buena practica eliminar archivos temporales o cosas que no son necesarias. Utilizar el archivo .dockerignore y por utilizar la imagen Alpine si esta disponible.

### Kubernetes

1. Hay que cambiar la definicion del pod (ya sea en el chart o mediante cli) y volver a deployar. Esto va a crear un pod nuevo y posteriormente destruir el anterior.

2. Antes de actualizar la version del cluster, revisaria que cambios tiene a diferencia de la version que esta actualmente, luego en un enotrno de prueba probaria que las aplicaciones funcionan correctamente por si hay que cambiar algo en manifiesto, ejemplo la referencia de la api. Tambien revisaria si tenemos la cantidad minima de ips que necesita el cluster de EKS para actualizar, ya que son requeridas (5 ips). Una vez chequeado todo esto, por medio de la consola de aws iniciaria el upgrade de version. Tambien es posible actualizar la version con eksctl.

### AWS

1. Para poder acceder a recursos entre cuentas de AWS hay que utilizar roles de cross account, lo cual la cuenta A asume roles de la cuenta B. Estos roles tienen que tener configurada la relacion de confianza (trusted relationship), junto a la policy sts:assumerole.

2. Primero generaria un role principal en la cuenta de shared service account.
   Luego en cada enviroment account generaria un role con las policies necesarias y configurando trusted relationship con el role de la cuenta service account. Por ultimo utilizaria workspaces de terraform para desplegar en las distintas cuentas.

### CI/CD

1. Pipeline ideal:
   ![Cicd](./src/diagram2.png)

2. Anti patrones:

   - `Aprovisionamiento de datos no similares a la producción.`
   - `Incosistencias entre el ambiente UAT y Prod.`
   - `Mucha Deuda Tecnica`
   - `Cambios costosos y lentos.`

3. GitFlow y OneFlow

4. Continuous Integration, Continuous Delivery y Continuous Deployment

- `Continuous Integration, significa que el codigo de todos se mergea frecuentemente, al menos una vez por dia. De esta forma se manetiene el repositorio de codigo estable desde donde cualquiera puede empezar a trabajar en algun nuevo cambio. En el se tiene automatizados los build y distintos test/chequeos automaticos.`

- `Continuous Delivery es la liberacion continua al cliente lo mas seguido posible. Lo que se acostumbra a hacer es integracion continua, se arma con frecuencia una version del sistema con el ultimo codigo integrado, se realizan chequeos automaticos y si pasan dichos chequeos se termina realizando la instalacion en un ambiente de prueba.`

- `Continuous Deployment este ultimo, si el codigo nuevo pasa todos los chequeos automaticos en el ambiente de pruebas, entonces ahi pasa al ambiente productivo.`
