terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 4"
    }
  }
  backend "s3" {
    region         = "us-east-1"
    bucket         = "terraform-state-storage-devops-challenge"
    key            = "devops-challenge.tfstate"
    dynamodb_table = "terraform-state-lock-devops-challenge"
    profile        = "devops-challenge-keys"
    encrypt        = true
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.profile
}
