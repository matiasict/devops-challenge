locals {
  account_id = data.aws_caller_identity.current.account_id
  default_tags = {
    Environment = var.environment
    Owner       = var.owner
  }
}
