module "vpc" {
  source = "../terraform-modules/terraform-aws-vpc"

  name                 = var.name
  environment          = var.environment
  owner                = var.owner
  cidr                 = var.cidr_block
  private_subnets      = [cidrsubnet(var.cidr_block, 2, 0), cidrsubnet(var.cidr_block, 2, 1)]
  public_subnets       = [cidrsubnet(var.cidr_block, 2, 2), cidrsubnet(var.cidr_block, 2, 3)]
  azs                  = var.availability_zones
  enable_dns_hostnames = true
  enable_nat_gateway   = false
  enable_dns_support   = true
  tags                 = local.default_tags
}
