variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "profile" {
  type    = string
  default = "devops-challenge-keys"
}

variable "name" {
  type    = string
  default = "devops-challenge"
}

variable "environment" {
  type    = string
  default = "sandbox"
}

variable "owner" {
  type    = string
  default = "infrastructure"
}

variable "cidr_block" {
  type    = string
  default = "172.16.0.0/16"
}

variable "availability_zones" {
  default = ["us-east-1a", "us-east-1b"]
}
