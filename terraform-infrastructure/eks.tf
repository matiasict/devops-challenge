# locals {
#   cluster_name = "devops-challenge-${random_string.suffix.result}"
#   node_group   = "workers-${random_string.suffix.result}"
# }

# resource "random_string" "suffix" {
#   length  = 8
#   special = false
# }

# resource "aws_eks_cluster" "eks-cluster" {
#   name                      = local.cluster_name
#   role_arn                  = aws_iam_role.cluster-role.arn
#   enabled_cluster_log_types = ["api", "audit"]
#   vpc_config {
#     subnet_ids = concat(module.vpc.public_subnets, module.vpc.private_subnets)
#   }
#   version = "1.23"

#   depends_on = [
#     aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
#     aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
#   ]
# }

# resource "aws_iam_role" "cluster-role" {
#   name = "eks-cluster-role-pin"

#   assume_role_policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "eks.amazonaws.com"
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# POLICY
# }

# resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
#   role       = aws_iam_role.cluster-role.name
# }

# resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
#   role       = aws_iam_role.cluster-role.name
# }


# #OUTPUTS

# resource "aws_eks_node_group" "workers" {
#   cluster_name    = aws_eks_cluster.eks-cluster.name
#   node_group_name = local.node_group
#   node_role_arn   = aws_iam_role.node-group-role.arn
#   subnet_ids      = concat(module.vpc.public_subnets, module.vpc.private_subnets)

#   scaling_config {
#     desired_size = 2
#     max_size     = 2
#     min_size     = 1
#   }
#   instance_types = ["t2.small"]
#   disk_size      = "10"
#   capacity_type  = "ON_DEMAND"

#   update_config {
#     max_unavailable = 1
#   }

#   depends_on = [
#     aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
#     aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
#     aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
#   ]
# }

# resource "aws_iam_role" "node-group-role" {
#   name = "eks-node-group-pin"

#   assume_role_policy = jsonencode({
#     Statement = [{
#       Action = "sts:AssumeRole"
#       Effect = "Allow"
#       Principal = {
#         Service = "ec2.amazonaws.com"
#       }
#     }]
#     Version = "2012-10-17"
#   })
# }

# resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
#   role       = aws_iam_role.node-group-role.name
# }

# resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
#   role       = aws_iam_role.node-group-role.name
# }

# resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
#   role       = aws_iam_role.node-group-role.name
# }
