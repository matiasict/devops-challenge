resource "aws_ecr_repository" "repo" {
  name                 = "website"
  image_tag_mutability = "IMMUTABLE"
}

resource "aws_ecr_repository_policy" "repo-policy" {
  repository = aws_ecr_repository.repo.name
  policy     = data.aws_iam_policy_document.source.json
}

data "aws_iam_policy_document" "source" {
  statement {
    sid = "adds full ecr access to the demo repository"

    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:CompleteLayerUpload",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetLifecyclePolicy",
      "ecr:InitiateLayerUpload",
      "ecr:PutImage",
      "ecr:UploadLayerPart"
    ]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.account_id}:root"]
    }
  }
}
