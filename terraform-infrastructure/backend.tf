module "remote-state" {
  source = "../terraform-modules/terraform-backend"

  state_name  = var.name
  environment = var.environment
}
